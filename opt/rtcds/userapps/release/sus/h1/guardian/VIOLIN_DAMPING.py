# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-

import time
import lscparams
import ISC_library
import ISC_GEN_STATES
from guardian import GuardState, GuardStateDecorator, NodeManager
import fast_ezca as fez

# nominal = 'IDLE' JSK for ER13
nominal = 'DAMP_VIOLINS_FULL_POWER'

def adjust_gain_func():
    nominal_gain = lscparams.vio_settings[sus][str(mode)]['gain']
    #how many orders of magnitude is the mode rung up above what we expect? assuming 5 is the monitor level for which the gain is set.
    amp = 10**(ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] - lscparams.vio_mon['Nominal'])
    calc_gain = round(nominal_gain/amp,3)
    ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).ramp_gain(calc_gain, 10, wait=False)
#################################################
# STATES
#################################################

class IDLE(GuardState):
    index = 2
    def run(self):

        return True

class CLEAR_MONITORS(GuardState):
    index = 4
    def main(self):
        for sus in ['ITMX', 'ITMY', 'ETMX', 'ETMY']:
            for mode in range(1,11): #until we have all the monitors
                ezca['SUS-%s_L2_DAMP_MODE%s_BL_RSET'%(sus, mode)] = 2
        #set a timer long enough to let the filters settle
        self.timer['history'] = 160
    def run(self):
        if self.timer['history']:
            return True
##################################################
# Violin mode damping
##################################################
class DAMPING_SETTINGS(GuardState):
    request = False
    index = 3
    @ISC_library.assert_dof_locked_gen(['IMC', 'DRMI'])
    def main(self):

        mtrx_dict = {'L': 1, 'P': 2, 'Y': 3, 'TST':4}
        #before doing set up, make sure all damping is off in case people manually engaged, this is to prevent the problem 
        for sus in ['ITMX', 'ITMY', 'ETMX', 'ETMY']:
            for mode in range(1,41):
                ezca['SUS-%s_L2_DAMP_MODE%s_TRAMP'%(sus,mode)] = 5
            for mode in range(1,41):
                ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus,mode)] = 0                
        time.sleep(5)

        # Set up filter banks
        for sus in ['ITMX', 'ITMY', 'ETMX', 'ETMY']:
            #get the setings for this test mass
            modes = lscparams.vio_settings[sus]
            #set the settings for the modes that are damped on this test mass
            for mode, setting in modes.items(): # iterates through modes in lscparams
                modeTuple = ('INPUT', 'OUTPUT', 'DECIMATION') + tuple( modes[str(mode)]['FMs'] )
                ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).only_on(*modeTuple)
                ezca['SUS-%s_L2_DAMP_MODE%s_TRAMP'%(sus,mode)] = 5
                for dof in ['L','P','Y']:  # Need to include 'TST' after all suspensions have L3 damping
                    if dof in modes[str(mode)]['drive']:
                        ezca['SUS-%s_L2_DAMP_MODE_MTRX_%s_%s'%(sus,mtrx_dict[dof],mode)] = 1
                    else:
                        ezca['SUS-%s_L2_DAMP_MODE_MTRX_%s_%s'%(sus,mtrx_dict[dof],mode)] = 0
                # Add the lscparams gain values to the NORMALGAIN & MAX_GAIN chan  (these are used for montiros on medm screens)
                ezca['SUS-{}_L2_DAMP_MODE{}_NOMINALGAIN'.format(sus, mode)] = setting['gain']
                ezca['SUS-{}_L2_DAMP_MODE{}_MAXGAIN'.format(sus, mode)] = setting['max_gain']

        # Turn filter bank inputs back on, since they now get turned off in PREP_FOR_LOCKING
        do_list = []
        for optic in ['ITMX','ETMY','ETMX','ITMY']:
            for mode in range(1,41):
                do_list.append(('switch', 'SUS-%s_L2_DAMP_MODE%s'%(optic,mode), 'INPUT', 'ON'))
        fez.do_many(ezca, do_list)
        #set a 5 minutes timer, now that we've set the filters wait 5 minutes for the filter transient to settle before we turn on the damping
        self.timer['filter pause'] = 300

    @ISC_library.assert_dof_locked_gen(['IMC', 'DRMI'])
    def run(self):
        if self.timer['filter pause']:
            return True

class DAMPING_SETTINGS_2W(GuardState):
    request = False
    index = 5
    @ISC_library.assert_dof_locked_gen(['IMC', 'DRMI'])
    def main(self):

        mtrx_dict = {'L': 1, 'P': 2, 'Y': 3, 'TST':4}
        #before doing set up, make sure all damping is off in case people manually engaged, this is to prevent the problem 
        for sus in ['ITMX', 'ITMY', 'ETMX', 'ETMY']:
            for mode in range(1,41):
                ezca['SUS-%s_L2_DAMP_MODE%s_TRAMP'%(sus,mode)] = 5
            for mode in range(1,41):
                ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus,mode)] = 0                
        time.sleep(5)

        # Set up filter banks
        for sus in ['ITMX', 'ITMY', 'ETMX', 'ETMY']:
            #get the setings for this test mass
            modes = lscparams.vio_settings[sus]
            #set the settings for the modes that are damped on this test mass
            for mode in modes.keys(): # iterates through modes in lscparams
                if '2W' in modes[mode].keys():
                    setting = modes[mode]['2W']
                else:
                    setting = modes[mode]
                
                modeTuple = ('INPUT', 'OUTPUT', 'DECIMATION') + tuple( setting['FMs'] )
                ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).only_on(*modeTuple)
                ezca['SUS-%s_L2_DAMP_MODE%s_TRAMP'%(sus,mode)] = 5
                for dof in ['L','P','Y']:  # Need to include 'TST' after all suspensions have L3 damping
                    if dof in setting['drive']:
                        ezca['SUS-%s_L2_DAMP_MODE_MTRX_%s_%s'%(sus,mtrx_dict[dof],mode)] = 1
                    else:
                        ezca['SUS-%s_L2_DAMP_MODE_MTRX_%s_%s'%(sus,mtrx_dict[dof],mode)] = 0
                # Add the lscparams gain values to the NORMALGAIN & MAX_GAIN chan  (these are used for montiros on medm screens)
                ezca['SUS-{}_L2_DAMP_MODE{}_NOMINALGAIN'.format(sus, mode)] = setting['gain']
                ezca['SUS-{}_L2_DAMP_MODE{}_MAXGAIN'.format(sus, mode)] = setting['max_gain']

        # Turn filter bank inputs back on, since they now get turned off in PREP_FOR_LOCKING
        do_list = []
        for optic in ['ITMX','ETMY','ETMX','ITMY']:
            for mode in range(1,41):
                do_list.append(('switch', 'SUS-%s_L2_DAMP_MODE%s'%(optic,mode), 'INPUT', 'ON'))
        fez.do_many(ezca, do_list)
        #set a 5 minutes timer, now that we've set the filters wait 5 minutes for the filter transient to settle before we turn on the damping
        self.timer['filter pause'] = 300

    @ISC_library.assert_dof_locked_gen(['IMC', 'DRMI'])
    def run(self):
        if self.timer['filter pause']:
            return True
            
def ENGAGE_DAMPING(susList):
    class ENGAGE_DAMPING(GuardState):
        request = False
        #just engages dampingwithout checking stuff
        @ISC_library.assert_dof_locked_gen(['Full_IFO'])
        def main(self):

            mtrx_dict = {'L': 1, 'P': 2, 'Y': 3}

            # Set up filter banks
            for sus in susList:
                #get the setings for this test mass
                modes = lscparams.vio_settings[sus]
                for mode in modes:
                    #check that the monitor isn't so high that we should engage damping with low gain
                    if ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] > lscparams.vio_mon['Rung_up'] :
                        log('MODE%s on %s is rung up so much we are reducing the gain we use'%(mode, sus))
                        nominal_gain = lscparams.vio_settings[sus][str(mode)]['gain']
                        #how many orders of magnitude is the mode rung up above what we expect? assuming 5 is the monitor level for which the gain is set.
                        amp = 10**(ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] - lscparams.vio_mon['Nominal'])
                        calc_gain = round(nominal_gain/amp,3)
                        ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).ramp_gain(calc_gain, 10, wait=False)
                    #check based on the monitor if this mode is rung up enough to damp in RF DARM
                    elif ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] > lscparams.vio_mon['DC_noise_floor'] :
                        ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).ramp_gain(lscparams.vio_settings[sus][str(mode)]['gain'], 10, wait=False)
                    else:
                        log('MODE%s on %s is not rung up enough to damp'%(mode, sus))


        @ISC_library.assert_dof_locked_gen(['Full_IFO'])
        def run(self):
            return True

    return ENGAGE_DAMPING

def ENGAGE_DAMPING_2W(susList):
    class ENGAGE_DAMPING_2W(GuardState):
        request = False

        @ISC_library.assert_dof_locked_gen(['Full_IFO'])
        def main(self):

            mtrx_dict = {'L': 1, 'P': 2, 'Y': 3}

            # Set up filter banks
            for sus in susList:
                #get the setings for this test mass
                modes = lscparams.vio_settings[sus]
                for mode in modes:
                    #check that the monitor isn't so high that we should engage damping with low gain
                    if ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] > lscparams.vio_mon['Rung_up'] :
                        log('MODE%s on %s is rung up so much we are reducing the gain we use by factor of 100'%(mode, sus))
                        ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).ramp_gain(lscparams.vio_settings[sus][str(mode)]['gain']/100, 5, wait=False)
                    #check based on the monitor if this mode is rung up enough to damp at 2W
                    elif ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] > 3 :
                        if '2W' in modes.keys():
                            ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).ramp_gain(lscparams.vio_settings[sus][str(mode)]['2W']['gain'], 10, wait=False)
                        else:
                            ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).ramp_gain(lscparams.vio_settings[sus][str(mode)]['gain'], 10, wait=False)
                    else:
                        log('MODE%s on %s is not rung up enough to damp at 2W'%(mode, sus))

        @ISC_library.assert_dof_locked_gen(['Full_IFO'])
        def run(self):
            return True

    return ENGAGE_DAMPING_2W

def ENGAGE_DAMPING_RF(susList):
    class ENGAGE_DAMPING_RF(GuardState):
        request = False

        @ISC_library.assert_dof_locked_gen(['Full_IFO'])
        def main(self):

            mtrx_dict = {'L': 1, 'P': 2, 'Y': 3}

            # Set up filter banks
            for sus in susList:
                #get the setings for this test mass
                modes = lscparams.vio_settings[sus]
                for mode in modes:
                    #check that the monitor isn't so high that we should engage damping with low gain
                    if ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] > lscparams.vio_mon['Rung_up'] :
                        log('MODE%s on %s is rung up so much we are reducing the gain we use by factor of 100'%(mode, sus))
                        ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).ramp_gain(lscparams.vio_settings[sus][str(mode)]['gain']/100, 5, wait=False)
                    #check based on the monitor if this mode is rung up enough to damp in RF DARM
                    elif ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] > lscparams.vio_mon['RF_noise_floor'] :
                        ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).ramp_gain(lscparams.vio_settings[sus][str(mode)]['gain'], 10, wait=False)
                    else:
                        log('MODE%s on %s is not rung up enough to damp while locked on RF'%(mode, sus))

        @ISC_library.assert_dof_locked_gen(['Full_IFO'])
        def run(self):
            return True

    return ENGAGE_DAMPING_RF



def DAMPING_ON_RF(susList):
    class DAMPING_ON_RF(GuardState):
        request = False
        @ISC_library.assert_dof_locked_gen(['Full_IFO'])
        def main(self):
            self.wd_flag = {}
            for sus in susList:
                self.wd_flag[sus] = False
        @ISC_library.assert_dof_locked_gen(['Full_IFO'])
        def run(self):
            for sus in susList:
                if not ezca['SUS-%s_BIO_L2_MON'%(sus)] == 26214:
                    #if L2 WD is tripped, turn off damping
                    self.wd_flag[sus] = True
                if self.wd_flag[sus] and ezca['SUS-%s_BIO_L2_MON'%(sus)] == 26214:
                    self.wd_flag[sus] = False
            for sus in susList:
                modes = lscparams.vio_settings[sus]
                for mode in range(1,41):
                    if self.wd_flag[sus]:
                        notify('WD tripped on %s, turning off damping'%(sus))
                        #if WD is tripped, turn off these modes
                        ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).ramp_gain(0, 5, wait=False)
                    #check based on the monitor if this mode is quiet enough to turn off the damping
                    elif ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] < lscparams.vio_mon['RF_noise_floor'] and not ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus,mode)] == 0:
                        ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).ramp_gain(0, 5, wait=False)
                        log('MODE%s on %s is damped to the level of RF DARM noise'%(mode, sus))
            return True
    return DAMPING_ON_RF

DAMPING_ON_RF = DAMPING_ON_RF(['ITMX', 'ITMY', 'ETMX', 'ETMY'])
                
class DAMP_VIOLINS_FULL_POWER(GuardState):
    index = 37
    request = True
    @ISC_library.assert_dof_locked_gen(['Full_IFO'])
    def main(self):
        self.timer['monitor pause'] = 20*60
        self.timer['shortSleep'] = 3
        
        self.wd_flag = {}
        self.susList = ['ITMX', 'ITMY', 'ETMX', 'ETMY']
        for sus in self.susList:
                self.wd_flag[sus] = False
        self.mode_dict = {}
        for sus in self.susList:
            self.mode_dict[sus] = {}
            for mode in lscparams.vio_settings[sus]:
                #if the monitor starts too low (so that glitches will make it increase), instead start with a monitor level of 1
                if ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] <= 2:
                    in_use_mon = 2
                else:
                    in_use_mon = ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)]
                self.mode_dict[sus][mode] = { 'starting_mon' : ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)],
                                              'starting_gain': ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus,mode)],
                                              'done_damping':  False,
                                              'inUse_mon':     in_use_mon,
                                              'reset_number':  0,
                                              'guardian_gain': ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus,mode)],
                                              'guardian_damping': True} 

        self.timer['shortSleep'] = 3 # Wait this long between checking modes, so that people have time to change the gain if needed.



    @ISC_library.assert_dof_locked_gen(['Full_IFO'])
    def run(self):
    
        if self.timer['monitor pause']:
            
            for sus in self.susList:
                if not (ezca['SUS-%s_BIO_L2_MON'%(sus)] == 26214 or ezca['SUS-%s_BIO_L2_MON'%(sus)] == 13107 or ezca['SUS-%s_BIO_L2_MON'%(sus)] == 8738):
                    #if L2 WD is tripped, turn off damping
                    self.wd_flag[sus] = True
                else:
                    self.wd_flag[sus] = False
            if self.timer['shortSleep']:
                for sus in self.susList:
                    for mode in lscparams.vio_settings[sus]:
                        if self.wd_flag[sus]:
                            notify('WD tripped on %s, turning off damping'%(sus))
                            #if WD is tripped, turn off these modes
                            ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus,mode)).ramp_gain(0, 5, wait=False)


                        # If guardian is doing the damping, allow it to change gains. 
                        if self.mode_dict[sus][mode]['guardian_damping']:
                            if (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] - self.mode_dict[sus][mode]['inUse_mon']) > 1: # set this threshold back to 1, SED Dec 19 2022
                                #check if mode is growing
                                #log this turning off only once
                                if ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus, mode)] != 0:
                                    log('%s mode %s is growing!!  turning off gain'%(sus, mode))
                                    log('inUse_mon %s'%(self.mode_dict[sus][mode]))
                                    log('current monitor level %s'%(ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)]))
                                    ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus, mode)] = 0
                                    self.mode_dict[sus][mode]['guardian_gain'] = 0 # Update the latest guardian gain
                                    
                           
                        if (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] < self.mode_dict[sus][mode]['inUse_mon']) and (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] > self.mode_dict[sus][mode]['starting_mon']):
                            self.mode_dict[sus][mode]['inUse_mon'] = ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)]
                        elif (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON'%(sus,mode)] < self.mode_dict[sus][mode]['starting_mon']):
                            self.mode_dict[sus][mode]['inUse_mon'] = self.mode_dict[sus][mode]['starting_mon']

                # Wait this long between checking modes, so that people have time to change the gain if needed.
                self.timer['shortSleep'] = 3



            for sus in self.susList:
                for mode in lscparams.vio_settings[sus]:
                    if self.mode_dict[sus][mode]['guardian_damping'] == False:
                        notify('%s %s gain is not at the Guardian value (%s)'%(sus, mode, self.mode_dict[sus][mode]['guardian_gain']))

            #log('ITMX mode11 %s'%(self.mode_dict['ITMX']['11']))
            
        return True


class DAMPING_ON_SIMPLE(GuardState):
    index = 40
    request = True
    @ISC_library.assert_dof_locked_gen(['Full_IFO'])
    def run(self):
        return True


def TURN_OFF_DAMPING(susList):
    class TURN_OFF_DAMPING(GuardState):
        goto = True
        def main(self):
            # I am not using get_LIGOFilter because I think that would wait for the ramp to ramp before going on to the next mode.
            # If we do wait= FAlse the gaurdian doesn't check that the ezca write actually worked, so we can try without it and see if that is fast enough
            #set all TRAMPs
            for optic in susList:
                for mode in range(1,41):
                    ezca['SUS-%s_L2_DAMP_MODE%s_TRAMP'%(optic,mode)] = 0.2
            # set all GAINs to zero
            for optic in susList:
                for mode in range(1,41):
                    ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(optic,mode)] = 0
            return True
        def run(self):
            return True

    return TURN_OFF_DAMPING

##############################
### Instances of generators
##############################

ENGAGE_DAMPING_ALL = ENGAGE_DAMPING(['ITMX', 'ITMY', 'ETMX', 'ETMY'])
ENGAGE_DAMPING_ALL.index = 10

ENGAGE_DAMPING_ITMX = ENGAGE_DAMPING(['ITMX'])
ENGAGE_DAMPING_ITMX.index = 11

ENGAGE_DAMPING_ITMY = ENGAGE_DAMPING(['ITMY'])
ENGAGE_DAMPING_ITMY.index = 12

ENGAGE_DAMPING_ETMX = ENGAGE_DAMPING(['ETMX'])
ENGAGE_DAMPING_ETMX.index = 13

ENGAGE_DAMPING_ETMY = ENGAGE_DAMPING(['ETMY'])
ENGAGE_DAMPING_ETMY.index = 14

ENGAGE_DAMPING_2W = ENGAGE_DAMPING_2W(['ITMX', 'ITMY', 'ETMX', 'ETMY'])
ENGAGE_DAMPING_2W.index = 6

ENGAGE_DAMPING_RF_ALL = ENGAGE_DAMPING_RF(['ITMX', 'ITMY', 'ETMX', 'ETMY'])
ENGAGE_DAMPING_RF_ALL.index = 20

ENGAGE_DAMPING_RF_ITMX = ENGAGE_DAMPING_RF(['ITMX'])
ENGAGE_DAMPING_RF_ITMX.index = 21

ENGAGE_DAMPING_RF_ITMY = ENGAGE_DAMPING_RF(['ITMY'])
ENGAGE_DAMPING_RF_ITMY.index = 22

ENGAGE_DAMPING_RF_ETMX = ENGAGE_DAMPING_RF(['ETMX'])
ENGAGE_DAMPING_RF_ETMX.index = 23

ENGAGE_DAMPING_RF_ETMY = ENGAGE_DAMPING_RF(['ETMY'])
ENGAGE_DAMPING_RF_ETMY.index = 24

TURN_OFF_DAMPING_ALL = TURN_OFF_DAMPING(['ITMX', 'ITMY', 'ETMX', 'ETMY'])
TURN_OFF_DAMPING_ALL.index = 30

TURN_OFF_DAMPING_ITMX = TURN_OFF_DAMPING(['ITMX'])
TURN_OFF_DAMPING_ITMX.index = 31

TURN_OFF_DAMPING_ITMY = TURN_OFF_DAMPING(['ITMY'])
TURN_OFF_DAMPING_ITMY.index = 32

TURN_OFF_DAMPING_ETMX = TURN_OFF_DAMPING(['ETMX'])
TURN_OFF_DAMPING_ETMX.index = 33

TURN_OFF_DAMPING_ETMY = TURN_OFF_DAMPING(['ETMY'])
TURN_OFF_DAMPING_ETMY.index = 34

#################################################
# EDGES
#################################################

edges = [('INIT', 'IDLE'),
    #states for damping when locked on RF
    ('IDLE', 'DAMPING_SETTINGS_2W'),
    ('DAMPING_SETTINGS_2W', 'ENGAGE_DAMPING_2W'),
    ('ENGAGE_DAMPING_2W', 'TURN_OFF_DAMPING_ALL'),
    ('IDLE','DAMPING_SETTINGS'),
    ('DAMPING_SETTINGS','ENGAGE_DAMPING_RF_ITMX'),
    ('ENGAGE_DAMPING_RF_ITMX','ENGAGE_DAMPING_RF_ITMY'),
    ('ENGAGE_DAMPING_RF_ITMY', 'ENGAGE_DAMPING_RF_ETMX'),
    ('ENGAGE_DAMPING_RF_ETMX', 'ENGAGE_DAMPING_RF_ETMY'),
    ('ENGAGE_DAMPING_RF_ETMY', 'DAMPING_ON_RF'),
    ('DAMPING_SETTINGS', 'CLEAR_MONITORS'),
    ('CLEAR_MONITORS','ENGAGE_DAMPING_RF_ALL'),
    ('ENGAGE_DAMPING_RF_ALL', 'DAMPING_ON_RF'),
    #states for damping on DC readout
    ('DAMPING_ON_RF', 'ENGAGE_DAMPING_ALL'),
    ('DAMPING_SETTINGS','ENGAGE_DAMPING_ALL'),
    ('DAMPING_SETTINGS', 'ENGAGE_DAMPING_ITMX'),
    ('ENGAGE_DAMPING_ITMX','ENGAGE_DAMPING_ITMY'),
    ('ENGAGE_DAMPING_ITMY', 'ENGAGE_DAMPING_ETMX'),
    ('ENGAGE_DAMPING_ETMX', 'ENGAGE_DAMPING_ETMY'),
    ('ENGAGE_DAMPING_ETMY', 'DAMP_VIOLINS_FULL_POWER'),
    ('DAMP_VIOLINS_FULL_POWER',       'DAMPING_ON_SIMPLE'),
    ('DAMPING_ON_SIMPLE',   'DAMP_VIOLINS_FULL_POWER'),
    # states for turning off damping
    ('TURN_OFF_DAMPING_ALL', 'IDLE'),
    ('TURN_OFF_DAMPING_ITMX', 'IDLE'),
    ('TURN_OFF_DAMPING_ITMY', 'IDLE'),
    ('TURN_OFF_DAMPING_ETMX', 'IDLE'),
    ('TURN_OFF_DAMPING_ETMY', 'IDLE'),
    ]
